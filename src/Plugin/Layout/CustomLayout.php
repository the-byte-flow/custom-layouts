<?php

namespace Drupal\custom_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Provides a Custom layout class.
 */
class CustomLayout extends LayoutDefault implements PluginFormInterface, ContainerFactoryPluginInterface
{
  const SECTION_COLORS_CONSTANT_NAME = 'SECTIONS_COLORS';
  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return parent::defaultConfiguration() + [
        'title' => '',
        'title_element' => '',
        'title_classes' => '',
        'section_classes' => '',
        'section_identifier' => '',
        'container_class' => 'container',
        'row_classes' => '',
        'section_background' => '',
        'spacing' => [
          'margin' => '',
          'padding' => '',
        ]
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $configuration = $this->getConfiguration();
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $configuration['title'],
      '#description' => $this->t('Custom title for this section.'),
    ];

    $form['title_element'] = [
      '#type' => 'select',
      '#title' => $this->t('Title element'),
      '#default_value' => !empty($configuration['title_element']) ? $configuration['title_element'] : 'h2',
      '#options' => [
        'span' => $this->t('Span'),
        'div' => $this->t('Div'),
        'h1' => $this->t('Heading 1'),
        'h2' => $this->t('Heading 2'),
        'h3' => $this->t('Heading 3'),
        'h4' => $this->t('Heading 4'),
      ],
      '#description' => $this->t('Element for the custom title.'),
      '#states' => [
        'visible' => [
          ':input[name="layout_settings[title]"]' => ['filled' => TRUE],
        ],
      ],
    ];

    $form['title_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title classes'),
      '#default_value' => $configuration['title_classes'],
      '#description' => $this->t('Select classes for the title.'),
      '#states' => [
        'visible' => [
          ':input[name="layout_settings[title]"]' => ['filled' => TRUE],
        ],
      ],
    ];

    $form['section_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Section classes'),
      '#default_value' => $configuration['section_classes'],
      '#description' => $this->t('Select classes for the section.'),
    ];

    $form['section_identifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Section identifier'),
      '#default_value' => $configuration['section_identifier'],
      '#description' => $this->t('Write the section identifier (optional)'),
    ];

    $form['container_class'] = [
      '#type' => 'radios',
      '#title' => $this->t('Container'),
      '#default_value' => $configuration['container_class'],
      '#description' => $this->t('Select the desired type of container'),
      '#options' => [
        'container' => 'Normal',
        'container-fluid' => 'Fluid',
        'container-fluid-left' => 'Fluid left',
        'container-fluid-right' => 'Fluid right',
      ],
    ];

    $form['row_classes'] = [
      '#type' => 'radios',
      '#title' => $this->t('Row'),
      '#default_value' => $configuration['row_classes'] ?? '',
      '#description' => $this->t('Select the desired type of the row'),
      '#options' => [
        '' => 'None',
        'no-gutters' => 'No gutters',
      ],
    ];


    $spacing = $configuration['spacing'] ?? ['padding' => '', 'margin' => ''];

    $form['spacing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Spacing'),
    ];

    $form['spacing']['padding'] = [
      '#type' => 'select',
      '#title' => $this->t('Internal'),
      '#options' => [
        '' => $this->t('None'),
        'py-3' => $this->t('Both: Small'),
        'py-4' => $this->t('Both: Medium'),
        'py-5' => $this->t('Both: Large'),
        'pt-3' => $this->t('Top: Small'),
        'pt-4' => $this->t('Top: Medium'),
        'pt-5' => $this->t('Top: Large'),
        'pb-3' => $this->t('Bottom: Small'),
        'pb-4' => $this->t('Bottom: Medium'),
        'pb-5' => $this->t('Bottom: Large'),
      ],
      '#default_value' => $spacing['padding'] ?? '',
    ];

    $form['spacing']['margin'] = [
      '#type' => 'select',
      '#title' => $this->t('External'),
      '#options' => [
        '' => $this->t('None'),
        'my-3' => $this->t('Both: Small'),
        'my-4' => $this->t('Both: Medium'),
        'my-5' => $this->t('Both: Large'),
        'mt-3' => $this->t('Top: Small'),
        'mt-4' => $this->t('Top: Medium'),
        'mt-5' => $this->t('Top: Large'),
        'mb-3' => $this->t('Bottom: Small'),
        'mb-4' => $this->t('Bottom: Medium'),
        'mb-5' => $this->t('Bottom: Large'),
      ],
      '#default_value' => $spacing['margin'] ?? '',
    ];

    $baseColors = [
      '' => $this->t('None'),
      'bg-light' => $this->t('Light'),
      'bg-dark' => $this->t('Dark'),
      'bg-primary' => $this->t('Primary'),
      'bg-secondary' => $this->t('Secondary'),
    ];

    $extraColors = $this->getExtraColors();

    $form['section_background'] = [
      '#type' => 'select',
      '#title' => $this->t('Background'),
      '#options' => array_merge($baseColors, $extraColors),
      '#default_value' => $configuration['section_background'] ?? '',
    ];

    $form['hide_if_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide section if empty'),
      '#default_value' => $configuration['hide_if_empty'] ?? false,
    ];

    return $form;
  }

  public function getExtraColors()
  {
    if (!\Drupal::hasService('tbf.app_handler')) {
      return [];
    }

    $appHandler = \Drupal::service('tbf.app_handler');

    if (!defined(sprintf('%s::LAYOUT_BUILDER_SECTIONS_COLORS', get_class($appHandler)))) {
      return [];
    }

    return $appHandler::LAYOUT_BUILDER_SECTIONS_COLORS;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    // Any additional form validation that is required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $this->configuration['title'] = $form_state->getValue('title');
    $this->configuration['title_element'] = $form_state->getValue('title_element');
    $this->configuration['title_classes'] = $form_state->getValue('title_classes');
    $this->configuration['section_classes'] = $form_state->getValue('section_classes');
    $this->configuration['section_identifier'] = $form_state->getValue('section_identifier');
    $this->configuration['container_class'] = $form_state->getValue('container_class');
    $this->configuration['row_classes'] = $form_state->getValue('row_classes');
    $this->configuration['spacing'] = $form_state->getValue('spacing');
    $this->configuration['section_background'] = $form_state->getValue('section_background');
    $this->configuration['hide_if_empty'] = $form_state->getValue('hide_if_empty');
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions)
  {
    // Build the render array as usual.
    $build = parent::build($regions);

    $configuration = $this->getConfiguration();
    $sectionClasses = $configuration['section_classes'] ?? '';
    $sectionClasses = sprintf('%s %s %s %s', $sectionClasses, $configuration['spacing']['padding'], $configuration['spacing']['margin'], $configuration['section_background'] ?? '');


    $build['#settings']['title'] = $configuration['title'] ?? '';
    $build['#settings']['title_element'] = $configuration['title_element'] ?? 'h2';
    $build['#settings']['title_classes'] = $configuration['title_classes'] ?? '';
    $build['#settings']['section_classes'] = $sectionClasses;
    $build['#settings']['section_identifier'] = $configuration['section_identifier'] ?? '';;
    $build['#settings']['container_class'] = $configuration['container_class'] ?? 'container';
    $build['#settings']['row_classes'] = $configuration['row_classes'] ?? '';
    $build['#settings']['hide_if_empty'] = $configuration['hide_if_empty'] ?? false;

    return $build;
  }

}
